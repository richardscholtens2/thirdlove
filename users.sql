CREATE TABLE users (
    user_id INT(11) AUTO_INCREMENT,
    name VARCHAR(100),
    password_hash CHAR(60),
    discription VARCHAR(500),
    email VARCHAR(250),
    firstname VARCHAR(100),
    lastname VARCHAR(100),
    gender VARCHAR(20),
    bdate DATE,
    pic VARCHAR(100),
    PRIMARY KEY (user_id)
);
