CREATE TABLE messages (
    message_id INT(11) AUTO_INCREMENT,
    message VARCHAR(500),
    sender VARCHAR(100),
    receiver VARCHAR(100),
    PRIMARY KEY (message_id)
);
