<?php
// For debugging:
error_reporting(-1);
ini_set("display_errors", 1);

// Read database credentials from configuration file:
require_once('config.inc.php');

// Create a database connection:
$dbc = new PDO("mysql:dbname=$db_name;host=$db_host",
               $db_user, $db_pass,
               [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

// Get request parameters:
$message = $_POST['message'];
$sender = $_POST['sender'];
$receiver = $_POST['receiver'];

// Insert product:
$qh = $dbc->prepare('INSERT INTO messages (message, sender, receiver)
                     VALUES (?, ?, ?)');
$qh->execute([$message, $sender, $receiver]);

// Redirect to product list:
header('Location: .');
?>
