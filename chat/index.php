<?php
session_start();
error_reporting(-1);
ini_set("display_errors", 1);
?>
<!DOCTYPE html>
<html>
<head>
    <title>Thirdlove | Chat</title>
    <link href="style.css" media="all" rel="stylesheet" type="text/css"/>
    <link href="../main.css" media="all" rel="stylesheet" type="text/css"/>
</head>
<body>
    <?php
        $user = $_SESSION['username'];
        $chatpartner = $_SESSION['chatpartner'];
    ?>
    <div id="header">

    </div>




    <section id='wrapper'>
    <section id='chatheader'>
        Chatting with: <?=$chatpartner?><br><a href='../index.php'>Back to homepage.</a>
    </section>
    <section id='wrapper2'>

    <section id='messages'>
        <?php
            // Read database credentials from configuration file:
            require_once('config.inc.php');

            // Create a database connection:
            $db = new PDO("mysql:dbname=$db_name;host=$db_host",
                    $db_user, $db_pass,
                    [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

            // Retrieve products from the items table:
            $query = $db->prepare('SELECT message_id, message, sender, receiver FROM messages');
            $query->execute();

            // Show them:
            foreach ($query as $row) {
                $id = $row['message_id'];
                $message = htmlspecialchars($row['message']);
                $sender = htmlspecialchars($row['sender']);
                $receiver = htmlspecialchars($row['receiver']);

                if (($sender==$user AND $receiver==$chatpartner) OR ($sender==$chatpartner AND $receiver==$user)) {
                    if ($sender==$user) {
        ?>
                        <li><section class='right'><?=$message?></section></li>
        <?php
                    }
                    else {
        ?>
                        <li><section class='left'><?=$message?></section></li>
        <?php
                    }
                }
            }
        ?>
    </section>
    <form action=add.php method=POST>
        <input type="text" name=message id=message maxlength=500 class='input' placeholder='Type your message here...'>
        <input type="hidden" name="receiver" value="<?=$chatpartner?>">
        <input type="hidden" name="sender" value="<?=$user?>">
        <input class='send' type="submit"  value=''>
    </form>
    </section>
    </section>
    <script type="text/javascript">
        document.getElementById("messages").scrollTop= 500000;
    </script>
</body>
</html>
