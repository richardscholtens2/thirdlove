CREATE TABLE posts (
    id INT(11) AUTO_INCREMENT,
    text VARCHAR(300),
    username VARCHAR(100),
    PRIMARY KEY (id)
);
