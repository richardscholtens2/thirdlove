# README #

In this readme you can find the necessary elements.

### What is this repository for? ###

* Social media platform for finding relationships or dates
* Online url: http://siegfried.webhosting.rug.nl/~s2644789/thirdlove/index.php

### How do I get set up? ###

* First you have to implement all the databases. Please run all the sql scripts
* Don't forget to add a confic.inc.php file
* From the index.php you can test the program. Please make an account and login.
* The folder chat: provide the code that you need for the chat function. A chat function is implemented so that people can interact with each other . NB.  Please refresh the chatbox for updates. At this stage it is not possible to fix the auto refresh. 
* The folder Users provide the code that is necessary for creating an account and provide the login form.
Also the code for the search function  is provided. 
* The folder Post provides the code that makes it possible to post messages on the newsfeed. Even it makes it possible to like profiles and messages. 


* NB. We re-structured the files. We hope that no folders or files got lost. For the working version visit the online-website http://siegfried.webhosting.rug.nl/~s2644789/thirdlove/index.php
NB. One problem occurs online if you upload a picture, automatic permissions are wrong. It should be the case that you are not allowed to see the pictures that you have uploaded. As far as I know it is not possible to change the automatic permissions. 


### Who do I talk to? ###

* Richard Scholten, Jordi Steegman, Robin Nicolai, Niels Eberson
* n.j.m.eberson@student.rug.nl