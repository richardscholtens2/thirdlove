<?php
error_reporting(-1);
ini_set("display_errors", 1);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../main.css">
    <title>Login</title>
  </head>
  <body>

    <div id="header">

    </div>


    <section id='wrapper'>
      <form action=login.php method=POST>
        <h2>Login to your account:</h2>
          <input type=text name=username id=username placeholder='Username' maxlength=100 >
          <input type=password name=password placeholder='Password' id=password maxlength=100 >
          <input type=submit value=Login>
      </form>
     No account yet?  <a href='create_form.php'>Register here.</a>
    </section>
  </body>
</html>
