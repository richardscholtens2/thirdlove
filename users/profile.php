<?php
session_start();
error_reporting(-1);
ini_set("display_errors", 1);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../main.css">
    <title>ThirdLove</title>
  </head>
  <body>

<section id='wrapper'>
    <ul>

        <?php
        require_once('../config.inc.php');

        $db = new PDO("mysql:dbname=$db_name;host=$db_host",
              $db_user, $db_pass,
              [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
              
        

	    if (isset($_POST['username']) && !empty($_POST['username'])) {
           	 $user = $_POST['username'];
		     $_SESSION['visiting'] = $_POST['username'];
  		     $_SESSION['chatpartner'] = $_POST['username'];
        }


        $query2 = $db->prepare('SELECT pic FROM users WHERE name = ?');
        $query2->execute([$_SESSION['visiting']]);
        
	    $stmt = $db->prepare('SELECT count(*) FROM userlikes WHERE liked_user = ?');
	    $stmt->execute([$_SESSION['visiting']]);
		$number_of_rows = $stmt->fetchColumn();
		echo "<a href='../index.php'>Back</a>";
   		echo "<h2>".$_SESSION['visiting']."'s profile </h2>";
        foreach ($query2 as $row) {
            $picture = htmlspecialchars($row['pic']);
            if (!empty($picture)) {
                echo "<img src='./uploads/".$picture."' class='profilepic'>";
            }
            else {
                echo "<img src='./uploads/hyves.jpg' class='profilepic'>";
            }
        }
        echo"<br><br>Page likes:<br>";
        echo "".$number_of_rows." likes";
		echo"<br>";
        $userfind = $db->prepare('SELECT count(*) FROM userlikes WHERE liked_user= ? AND liking_user= ?');
	    $userfind->execute([$_SESSION['visiting'], $_SESSION['username']]);
		$likecount = $userfind->fetchColumn();
		if ($likecount > 0){
			echo "
			    <form action=userunlike.php method=POST>
               	    <input type=hidden name=user value=".$_SESSION['visiting'].">
                    <input type=submit value=unlike>
                </form>
			    <br>";
		}else{
            echo "
			    <form action=userlike.php method=POST>
               	    <input type=hidden name=user value=".$_SESSION['visiting'].">
                    <input type=submit value=like>
                </form>
				<br>";
				}
        echo "
        <form action=../chat/index.php method=POST>
          <input type=hidden name=chatpartner value=".$_SESSION['visiting'].">
          <input type=submit value=chat>
        </form>";
        $visitinfo = $db->prepare('SELECT * FROM users WHERE name = ?');
        $visitinfo->execute([$_SESSION['visiting']]);
        foreach ($visitinfo as $info) {
          $bdate = htmlspecialchars($info['bdate']);
          $desc = htmlspecialchars($info['discription']);
          $gender = htmlspecialchars($info['gender']);
        ?>
     Gender: <?=$gender?><br>
     Birthdate: <?=$bdate?><br>
     Description: <?=$desc?><br>
     <br>
     <br>
      <?php
    }


        $query = $db->prepare('SELECT * FROM posts WHERE username = ?');
        $query->execute([$_SESSION['visiting']]);
        echo"Posts:";
		echo"<br>";
	    echo"_______________________________________";
	    echo"<br>";
        foreach ($query as $row) {
          $id = $row['id'];
          $text = htmlspecialchars($row['text']);
        ?>
		 <?=$text?>
         <br>
		 <br>
      <?php
    }
    ?>

    </ul>
</section>
  </body>
</html>
