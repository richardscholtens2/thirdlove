<?php
session_start();
error_reporting(-1);
ini_set("display_errors", 1);

require_once('../config.inc.php');

$dbh = new PDO("mysql:dbname=$db_name;host=$db_host",
               $db_user, $db_pass,
               [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
			   
$username = $_SESSION['username'];
$user = $_POST['user'];

$qh = $dbh->prepare('INSERT INTO userlikes (liked_user, liking_user)
                     VALUES (?, ?)');
$qh->execute([$user, $_SESSION['username']]);  

header('Location: profile.php');
?>
