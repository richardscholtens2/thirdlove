<?php
session_start();
error_reporting(-1);
ini_set("display_errors", 1);

require_once('../config.inc.php');
require_once('../password.inc.php');

$dbc = new PDO("mysql:dbname=$db_name;host=$db_host",
               $db_user, $db_pass,
               [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

if (isset($_POST['username']) && isset($_POST['password'])){
	$query = $dbc->prepare('SELECT * FROM users WHERE name = ?');
	$query->execute(array($_POST['username']));
	$row = $query->fetch();
	if ($row && password_verify($_POST['password'], $row['password_hash'])){
		$_SESSION['userId'] = $row['user_id'];
        $_SESSION['username'] = $_POST['username'];
		header('Location: ../index.php');
	}
	else{
		header('Location: login_form.php');
	}
}

?>
