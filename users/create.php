<?php
session_start();
error_reporting(-1);
ini_set("display_errors", 1);

require_once('../config.inc.php');
require_once('../password.inc.php');

$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, choose another filename.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 5000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

$filename = basename($_FILES["fileToUpload"]["name"]);
$cnt = 0;
$name = $_POST['username'];
$pass = $_POST['password'];
$pass2 = $_POST['password2'];

$dbc = new PDO("mysql:dbname=$db_name;host=$db_host",
               $db_user, $db_pass,
               [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
$query1 = $dbc->prepare("SELECT * FROM users");
            $query1->execute();

foreach ($query1 as $row) {
    $username = htmlspecialchars($row['name']);
    if ($username==$name) {
      $cnt++;
    }
}

if ($cnt==0) {
    if (isset($_POST['username']) && isset($_POST['password'])){
        if ($pass==$pass2) {
            $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
            $query = $dbc->prepare('INSERT INTO users (name, password_hash, discription, email, gender, bdate, firstname, lastname, pic) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');
            $query->execute(array($_POST['username'], $password, $_POST['discription'], $_POST['email'], $_POST['gender'], $_POST['bdate'],$_POST['firstname'], $_POST['lastname'],$filename));
            header('Location: ../users/login_form.php');
        }
        else {
            header('Location: ./wrong_password.php');
        }
    }
}

else {
    header('Location: ./username_exists.php');
}
?>
