<?php
session_start();
error_reporting(-1);
ini_set("display_errors", 1);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../main.css">
    <title>ThirdLove</title>
  </head>
  <body>

<section id='wrapper'>
    <ul>

        <?php
        require_once('../config.inc.php');

        $db = new PDO("mysql:dbname=$db_name;host=$db_host",
              $db_user, $db_pass,
              [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
			   
	    $search = $_POST['usersearch'];

        $query = $db->prepare('SELECT name FROM users');
        $query->execute();
        echo"Results:"; 
        foreach ($query as $row) {
          $match = htmlspecialchars($row['name']);
		  $check = $db->prepare('SELECT LOCATE(?,?) as score');
          $check->execute([$search,$match]); 
		  foreach ($check as $result) {
		  if ($result['score'] > 0){
			?>
			    <form action=profile.php method=POST>
				   <input type=hidden name=username value=<?=$match?>>
                   <input type=submit value=<?=$match?>> 
                </form>
			<?php
		  }
		}
    }
    ?>

    </ul>
</section>
  </body>
</html>
