<?php
error_reporting(-1);
ini_set("display_errors", 1);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../main.css">
    <title>Create An Account</title>
  </head>
  <body>
    <div id="header">

    </div>
    <section id='wrapper'>
      <form action=create.php method=POST enctype="multipart/form-data">
        <h2>Create an account:</h2>
          Please think of a username and fill in your e-mail adress.<br>
          <input type=text name=username id=username placeholder='Username' maxlength=100 >
          <input type=email name=email id=email placeholder='xyz@hahaha.com' maxlength=90>

          <br>
          <i>Pick a different username!</i>
          <br>
          <br>
          Please think of a password and verify this password.<br>
          <input type=password name=password placeholder='Password' id=password maxlength=100 >
          <input type=password name=password2 placeholder='Retype Password' id=password2 maxlength=100 >

          <br>
          <br>
          Please describe yourself.<br>
          <input type=text name=discription id=discription placeholder='Describe yourself.' maxlength=500 class="discription">
          <br>
          <br>
          Please fill in your first and last name.<br>
          <input type=text name=firstname id=firstname placeholder='Firstname' maxlength=100 >
          <input type=text name=lastname id=lastname placeholder='Lastname' maxlength=100>
          <br>
          <br>
          Please fill in your gender.<br>
          <input type="radio" name="gender" id=gender value="male" checked> Male
          <br>
          <input type="radio" name="gender" id=gender value="female"> Female
          <br>
          <input type="radio" name="gender" id=gender value="other"> Other
          <br>
          <br>
          Please fill in your date of birth.<br>
          <input type="date" name="bdate" id=bdate placeholder='1997-01-08'>
          <br>
          <br>       
            Select image to upload:
            <input type="file" name="fileToUpload" id="fileToUpload">
            <input type="submit" value="Create account" name="submit">
      </form>

    </section>
  </body>
</html>
