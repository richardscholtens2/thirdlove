<?php
session_start();
error_reporting(-1);
ini_set("display_errors", 1);

require_once('../config.inc.php');

$dbh = new PDO("mysql:dbname=$db_name;host=$db_host",
               $db_user, $db_pass,
               [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
			   
$username = $_SESSION['username'];
$id = $_POST['id'];

$qh = $dbh->prepare('INSERT INTO postlikes (post_id, username)
                     VALUES (?, ?)');
$qh->execute([$id, $username]);  

header('Location: ../index.php');
?>
