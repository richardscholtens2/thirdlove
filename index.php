<?php
session_start();
error_reporting(-1);
ini_set("display_errors", 1);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="main.css">
    <title>thirdlove.nl</title>
  </head>
  <body>


  	<div id="header">

    </div>


    <section id='wrapper'>
    <h1>Timeline</h1>
    <ul>

        <?php
            if (isset($_SESSION['userId'])) {
                echo "Welcome " . $_SESSION['username'] . "! <a href=users/logout.php>Log out</a> ";
                require_once('config.inc.php');

                $db = new PDO("mysql:dbname=$db_name;host=$db_host",
                      $db_user, $db_pass,
                      [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
		?>
		    <br>
		    <form action=users/search.php method=POST>
                 <input type=text name=usersearch id=usersearch placeholder='Search User' maxlength=100 >
                 <input type=submit value=Search>
            </form>
		    <form action=posts/add.php method=POST>
                 <h2>Tell Your Affaires What You're Up To:</h2>
                 <input type=text name=post id=post style="width:500px" maxlength=300 >
	             <br>
                 <input type=submit value=Post>
            </form>
			<br>
			__________________________________________________________
			<br>
		<?php

		$query = $db->prepare('SELECT * FROM posts ORDER BY id DESC');
		$query->execute();
        	foreach ($query as $row) {
          	  $id = $row['id'];
          	  $text = htmlspecialchars($row['text']);
          	  $username = htmlspecialchars($row['username']);
			  $stmt = $db->prepare('SELECT count(*) FROM postlikes WHERE post_id = ?');
			  $stmt->execute([$id]);
			  $number_of_rows = $stmt->fetchColumn();
        	 ?>

         	 <li>
                 <form action=users/profile.php method=POST>
				   <input type=hidden name=username value=<?=$username?>>
                   <input type=submit value=<?=$username?>> says:
                </form>
                <?=$text?>
			    <?php
				if ($username  == $_SESSION['username']){
                       echo "
					 <form action=posts/delete.php method=POST>
               	        <input type=hidden name=id value=".$id.">
                        <input type=submit value=Delete>
                     </form>

			         <form action=posts/update_form.php method=POST>
               	         <input type=hidden name=id value=".$id.">
                         <input type=submit value=Edit>
                     </form>";
				}
		        ?>
				<br>
        <form action=users/likes.php method=POST>
          <input type=hidden name=id value=<?=$id?>>
          <input type=submit value="<?=$number_of_rows?> likes">
        </form>

			     <?php
				 $userfind = $db->prepare('SELECT count(*) FROM postlikes WHERE post_id= ? AND username= ?');
				 $userfind->execute([$id, $_SESSION['username']]);
				 $likecount = $userfind->fetchColumn();
				 if ($likecount > 0){
					 echo "
					    <form action=posts/unlike.php method=POST>
               	          <input type=hidden name=id value=".$id.">
                          <input type=submit value=unlike>
                        </form>
						<br>";
				 }else{
                    echo "
			            <form action=posts/like.php method=POST>
               	          <input type=hidden name=id value=".$id.">
                          <input type=submit value=like>
                        </form>
						<br>";
				}

				 ?>
             </li>


	<?php
                 }
        ?>

    </ul>

    </section>
    <section id='highscores'>
       <?php
       $scores = $db->prepare('SELECT * FROM (SELECT liked_user, COUNT(*) AS `num` FROM userlikes GROUP BY liked_user) AS T ORDER BY num DESC');
       $scores->execute();
       echo"Most Popular Users";
       echo"<br>";
       echo"<br>";
       $i=0;
            foreach ($scores as $high) {
               $theuser = htmlspecialchars($high['liked_user']);
               $userscore = htmlspecialchars($high['num']);
		?>
		     <form action=users/profile.php method=POST>
				   <input type=hidden name=username value=<?=$theuser?>>
                   <input type=submit value=<?=$theuser?>>-- <?=$userscore?> likes <br>
                </form>
<?php

$i++;
if($i==5) break;
            }

         }else {
   	      echo "Please <a href='users/login_form.php'>log in</a> first.";
         }
        ?>
</section>



  </body>
</html>
